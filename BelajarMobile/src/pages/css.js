import React from 'react'
import { View, Text, Image } from 'react-native'
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';

export default function Css() {
    return (
        <View style={{margin:15}}>
            <Image source={require("./assets/css.jpg")} style={{marginBottom:10}}/>
            <View>
                <Text style={{fontSize: 26}}>
                    Belajar CSS
                </Text>
                <View style={{marginTop:20}}>
                    <Text>CSS adalah bahasa Cascading Style Sheet dan biasanya digunakan untuk mengatur tampilan elemen yang tertulis dalam bahasa markup, seperti HTML. CSS berfungsi untuk memisahkan konten dari tampilan visualnya di situs. </Text>
                    
                </View>
                <Text style={{marginTop:15 ,fontSize: 26, marginBottom:5}}>
                    Materi Belajar CSS
                </Text>
                <View style={{ padding: 10 , borderWidth: 1, borderRadius: 20}}>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>1.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Pengenalan CSS</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   10.34</Text>
                        </View>
                    </View>
                    
                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>2.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Dasar pada CSS</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   10.21</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>3.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Macam-Macam CSS</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   11.57</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>4.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Membuat Layout CSS</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   12.34</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>5.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Membuat Tabel CSS</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   15.64</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>6.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Membuat List CSS</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   13.04</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>7.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Fitur CSS3</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   25.04</Text>
                        </View>
                    </View>
                </View>
            
            </View>
        </View>
    )
}