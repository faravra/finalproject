const Data =[
    {
        id : "1",
        judul: "Belajar HTML",
        image : require("./assets/html.jpg"),
        status : "Gratis",
        modul :"4 Modul",
        route : "html"
    },
    {
        id : "2",
        judul: "Belajar CSS",
        image: require("./assets/css.jpg"),
        status : "Gratis",
        modul :"5 Modul",
        route : "css"
    },
    {
        id : "3",
        judul: "Belajar JavaScript",
        image: require("./assets/javascript.jpg"),
        status : "Gratis",
        modul :"7 Modul",
        route : "javascript"
    },
]

export {Data};