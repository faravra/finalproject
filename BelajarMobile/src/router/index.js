import React from 'react';
import { View, Text } from 'react-native'
import {NavigationContainer} from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import GetStarted from '../pages/GetStarted';
import Login from '../pages/Login';
import Register from '../pages/Register';
import Home from '../pages/Home';
import Html from '../pages/html';
import Css from '../pages/css';
import Java from '../pages/javascript';
import Profile from '../pages/profile';

const Stack = createStackNavigator();

export default function Router() {
    return (
            <Stack.Navigator >
                <Stack.Screen component={GetStarted} name="GetStarted"/>
                <Stack.Screen component={Login} name="Login"/>
                <Stack.Screen component={Register} name="Register"/>
                <Stack.Screen component={Home} name="Home"/>
                <Stack.Screen component={Html} name="Html"/>
                <Stack.Screen component={Css} name="Css"/>
                <Stack.Screen component={Java} name="Javascript"/>
                <Stack.Screen component={Profile} name="Profile"/>
            </Stack.Navigator>
    )
    
}
