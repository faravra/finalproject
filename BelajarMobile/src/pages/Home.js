import React, {useEffect, useState, Component} from 'react'
import { StyleSheet, Text, View, Button, FlatList, Image} from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import * as firebase from 'firebase';
import { Ionicons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';

import firestore from '@react-native-firebase/firestore'


export default function Home({navigation}) {
    const [user, setUser] = useState({})

    useEffect(() => {
        const userInfo =firebase.auth().currentUser
        setUser(userInfo)
    }, [])



        return (
            <View style={styles.container}>
                <View style={{alignItems:'center', padding:5}}>
                    <TouchableOpacity style={{alignItems:'center',}} onPress={()=>navigation.navigate("Profile")} >
                        <Text style={{fontSize:24}}>Profile</Text>
                        <Ionicons name="person-circle" size={72} color="black" />
                        <Text>{user.email}</Text>
                    </TouchableOpacity>
                    
                </View>
                
                <Text style={{margin:20, fontSize:24}}>
                    Materi yang Tersedia
                </Text>

                <View style={{flexDirection: "row", justifyContent: "space-around"}}>
                    <View style={{alignItems:'center',  marginBottom: 0, paddingBottom: 0,}}>
                            <View style={{padding:10 ,}}>
                                <Text style={{textAlign: "center"}} >Belajar HTML</Text>
                                <Image source={require("./assets/html.jpg")}/>
                                <TouchableOpacity onPress={()=>navigation.navigate("Html")}>
                                    <Button color="grey" title="Mulai"/>
                                </TouchableOpacity>
                            </View>
                    </View>
                    <View style={{alignItems:'center',  marginBottom: 0, paddingBottom: 0}}>
                            <View style={{padding:10 ,}}>
                                <Text style={{textAlign: "center"}} >Belajar CSS</Text>
                                <Image source={require("./assets/css.jpg")}/>
                                <TouchableOpacity onPress={()=>navigation.navigate("Css")}>
                                    <Button color="grey" title="Mulai"/>
                                </TouchableOpacity>
                            </View>
                    </View>
                    <View style={{alignItems:'center',  marginBottom: 0, paddingBottom: 0}}>
                            <View style={{padding:10 ,}}>
                                <Text style={{textAlign: "center"}} >Belajar Javascript</Text>
                                <Image source={require("./assets/javascript.jpg")}/>
                                <TouchableOpacity onPress={()=>navigation.navigate("Javascript")}>
                                    <Button color="grey" title="Mulai"/>
                                </TouchableOpacity>
                            </View>
                    </View>
                </View>
                <Text style={{marginTop:20 ,fontSize: 26, margin:20}}>
                    Testimoni
                </Text>
                <View style={{ margin:10, padding: 10 , borderWidth: 1, borderRadius: 20}}>
                    <Text>Nama: Jaka Sembung</Text>
                    <Text>Baru kali ini ada pembelajaran SinauCoding, mudah dipelajari dan kapanpun dimanapun bisa memahami materinya</Text>
                </View>

                <View style={{ margin:10, padding: 10 , borderWidth: 1, borderRadius: 20}}>
                    <Text>Nama: Martha Pilar</Text>
                    <Text>Baru kali ini ada pembelajaran SinauCoding, mudah dipelajari dan kapanpun dimanapun bisa memahami materinya</Text>
                </View>

                <View style={{ margin:10, padding: 10 , borderWidth: 1, borderRadius: 20}}>
                    <Text>Nama: David Gonzel</Text>
                    <Text>Saya sudah membuktikan belajar di SinauCoding sangat lah mudah dipahami materinya, dan vidionya cocok sekali untuk belajar yang masih pemula</Text>
                </View>


            </View>
        )
    
    
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor: "#f2f2f2",
        // alignItems:'center',
        justifyContent:'center',
        marginTop: 10
    }
})
