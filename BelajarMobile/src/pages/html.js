import React from 'react'
import { View, Text, Image } from 'react-native'
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';

export default function Html() {
    return (
        <View style={{margin:15}}>
            <Image source={require("./assets/html.jpg")} style={{marginBottom:10}}/>
            <View>
                <Text style={{fontSize: 26}}>
                    Belajar HTML
                </Text>
                <View style={{marginTop:20}}>
                    <Text>HTML adalah singkatan dari Hypertext Markup Language. HTML memungkinkan seorang user untuk membuat dan menyusun bagian paragraf, heading, link atau tautan, dan blockquote untuk halaman web dan aplikasi. </Text>
                    <Text style={{marginTop:5}}>
                    HTML bukanlah bahasa pemrograman, dan itu berarti HTML tidak punya kemampuan untuk membuat fungsionalitas yang dinamis. Sebagai gantinya, HTML memungkinkan user untuk mengorganisir dan memformat dokumen, sama seperti Microsoft Word.
                    </Text>
                </View>
                <Text style={{marginTop:15 ,fontSize: 26, marginBottom:5}}>
                    Materi Belajar HTML
                </Text>
                <View style={{ padding: 10 , borderWidth: 1, borderRadius: 20}}>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>1.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Pengenalan HTML</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   10.34</Text>
                        </View>
                    </View>
                    
                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>2.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Dasar pada HTML</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   10.21</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>3.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Macam-Macam Tag HTML</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   11.57</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>4.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Struktur HTML</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   12.34</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>5.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Penerapan HTML</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   15.04</Text>
                        </View>
                    </View>
                </View>
            </View>
        </View>
    )
}
