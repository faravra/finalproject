import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View, Button, FlatList, Image} from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import * as firebase from 'firebase';
import { Ionicons } from '@expo/vector-icons';

export default function Profile({navigation}) {
    const [user, setUser] = useState({})

    useEffect(() => {
        const userInfo =firebase.auth().currentUser
        setUser(userInfo)
    }, [])
    const onLogout=()=>{
        firebase.auth()
        .signOut()
            .then(()=>{
                console.log('user Sign out');
                navigation.navigate('GetStarted')
            })
    }
    return (
        <View style={{margin:15}}>
            <View style={{marginLeft:20, marginRight:20}}>
                <Text>Setting</Text>
                    <Text>Hello, {user.email}</Text>
                </View>
                <View style={{width:70, marginLeft:20, marginRight:20}}>
                    <Button color="grey" onPress={onLogout} title="Logout"/>
            </View>
        </View>
    )
}
