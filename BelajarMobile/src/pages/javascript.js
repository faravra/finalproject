import React from 'react'
import { View, Text, Image } from 'react-native'
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';

export default function Javascript() {
    return (
        <View style={{margin:15}}>
            <Image source={require("./assets/javascript.jpg")} style={{marginBottom:10}}/>
            <View>
                <Text style={{fontSize: 26}}>
                    Belajar Javascript
                </Text>
                <View style={{marginTop:20}}>
                    <Text>JavaScript adalah bahasa pemrograman tingkat tinggi dan dinamis. JavaScript populer di internet dan dapat bekerja di sebagian besar penjelajah web populer seperti Google Chrome, Internet Explorer, Mozilla Firefox, Netscape dan Opera. Kode JavaScript dapat disisipkan dalam halaman web menggunakan tag SCRIPT. </Text>
                    
                </View>
                <Text style={{marginTop:15 ,fontSize: 26, marginBottom:5}}>
                    Materi Belajar Javascript
                </Text>
                <View style={{ padding: 10 , borderWidth: 1, borderRadius: 20}}>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>1.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Pengenalan Javascript</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   10.34</Text>
                        </View>
                    </View>
                    
                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>2.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Variable dan Tipe Data Javascript</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   10.21</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>3.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Operator Javascript</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   11.57</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>4.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Perulangan Javascript</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   12.34</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>5.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Pengkondisian Javascript</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   15.64</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>6.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Membuat List CSS</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   13.04</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>7.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Fungsi Javascript</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   13.24</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>8.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    Array Javascript</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   17.06</Text>
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between"}}>
                        <View style={{flexDirection: "row"}}>
                            <Text>9.  </Text>
                            <AntDesign name="caretright" size={18} color="black" />
                            <Text>    ES6 Javascript</Text>
                        </View>
                        <View style={{ flexDirection: "row"}}>
                        <Ionicons name="ios-videocam" size={20} color="black" />
                        <Text style={{paddingTop:2}}>   22.12</Text>
                        </View>
                    </View>
                </View>
            
            </View>
        </View>
    )
}
