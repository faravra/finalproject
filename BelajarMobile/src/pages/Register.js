import React, {useState, useEffect} from 'react'
import { Button, StyleSheet, Text, View } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import * as firebase from 'firebase';

export default function Register({navigation}) {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const firebaseConfig = {
        apiKey: "AIzaSyBwuLP5tVXFhBRi_D5efEvxsp3Dm8alpRo",
        authDomain: "tester-25cc4.firebaseapp.com",
        databaseURL: "https://tester-25cc4-default-rtdb.firebaseio.com",
        projectId: "tester-25cc4",
        storageBucket: "tester-25cc4.appspot.com",
        messagingSenderId: "452087039330",
        appId: "1:452087039330:web:5de301580679a20dc7183b"
    };
    if(!firebase.apps.length){
        firebase.initializeApp(firebaseConfig);
    }
    

    const submit=()=>{
        const data = {
            email,
            password
        }
        console.log(data)
        firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(()=>{
            // console.log('Register Berhasil')
            navigation.navigate("Home")
        }).catch(()=>{
            console.log("register gagal")
        })
    }

    return (
        <View style={styles.container}>
            <Text>Register</Text>
            <TextInput 
                style={styles.input}
                placeholder="Masukin Email"
                value={email}
                onChangeText={(value)=>setEmail(value)}

            />
            <TextInput 
                style={styles.input}
                placeholder="Masukin Password"
                value={password}
                onChangeText={(value)=>setPassword(value)}
            />
            <Button color="grey" onPress={submit} title="REGISTER"/>
            <TouchableOpacity style={{marginTop: 30}}
            onPress={()=>navigation.navigate("Login")}
            >
                <Text>Sudah Punya Akun</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor: "#f5f5f5",
    },
    input:{
        borderWidth:1,
        borderColor:'grey',
        paddingHorizontal:10,
        paddingVertical: 10,
        width: 300,
        marginBottom: 10,
        borderRadius: 6,
        marginTop: 10
    }
})
