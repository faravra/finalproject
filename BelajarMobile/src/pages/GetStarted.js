import React from 'react'
import { Button, StyleSheet, Text, View, Image } from 'react-native'
import { AntDesign } from '@expo/vector-icons';


export default function GetStarted({navigation}) {
    return (
        <View style={styles.container}>
            <AntDesign name="CodeSandbox" size={140} color="black" />
            <Text style={{fontSize: 24}}>Selamat Datang Di SinauCoding</Text>
            <View style={{marginVertical: 50}}/>
            <Button color="grey" onPress={()=>navigation.navigate("Login")}title="  Masuk Akun  "/>
            <View style={{marginVertical: 10}}/>
            <Button color="grey" onPress={()=>navigation.navigate("Register")}title="    Buat Akun    "/>
        </View>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor: "#f5f5f5",
    }
})
